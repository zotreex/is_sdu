package com.example.compose

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp


@Composable
fun Banner(title: String, subtitle: String, color: Color = Color.Blue) {
    Card(
        shape = RoundedCornerShape(8.dp),
        backgroundColor = color, modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        elevation = 8.dp
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Column(modifier = Modifier.align(Alignment.TopStart)) {
                Text(text = title)
                Spacer(modifier = Modifier.height(8.dp))
                Text(text = subtitle)
            }
            Icon(
                Icons.Filled.Close, modifier = Modifier
                    .height(16.dp)
                    .align(Alignment.TopEnd), contentDescription = "menu"
            )
        }
    }
}
