package com.example.compose.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.compose.R

@Composable
fun ServiceSellingListItem(isActive: Boolean, title: String, price: String) {
    Column(
        modifier = Modifier
            .clip(RoundedCornerShape(14.dp))
            .border(
                width = 2.dp,
                color = colorResource(id = if (isActive) R.color.brand else R.color.basic_silver),
                shape = RoundedCornerShape(14.dp)
            )
            .background(colorResource(id = R.color.white))
            .padding(16.dp)
    ) {
        Text(
            text = title,
            maxLines = 2,
            modifier = Modifier.padding(bottom = 10.dp).width(90.dp),
            color = colorResource(id = if (isActive) R.color.basic_content else R.color.basic_gray)
        )
        Text(
            text = price,
            color = colorResource(id = R.color.basic_gray)
        )
    }
}

@Preview
@Composable
fun PreviewServiceSellingListItem() {
    ServiceSellingListItem(isActive = false, title = "Умный\nдомофон", price = "49 ₽/мес")
}