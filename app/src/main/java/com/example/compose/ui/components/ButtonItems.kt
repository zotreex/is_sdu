package com.example.compose.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.compose.R
import com.example.compose.ui.models.UiButton


@Composable
fun LongButton(
    button: UiButton.LongButton
) {
    Button(
        onClick = {
            button.action
        },
        colors = ButtonDefaults.buttonColors(
            backgroundColor = button.backGroundColor
        ),
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(text = button.buttonText, color = button.textColor, fontSize = 16.sp)
    }
}

//@Composable
//fun SquaredButton(
//        button: UiButton.SquaredButton
//) {
//    Button(
//            onClick = {
//                button.action
//            },
//            colors = ButtonDefaults.buttonColors(
//                    backgroundColor = button.backGroundColor
//            ),
//            shape = RoundedCornerShape(5.dp)
//    ) {
//        Image(
//                painterResource(
//                        id = button.icon
//                ),
//                contentDescription = "Squared button icon",
//                modifier = Modifier.size(25.dp),
//                colorFilter = ColorFilter.tint(color = Color.Blue)
//        )
//    }
//}

@Composable
fun RingButton(
    button: UiButton.LockButton
) {
    Button(
        onClick = {},
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Red
        ),
        shape = RoundedCornerShape(50),
        modifier = Modifier
            .width(50.dp)
            .height(50.dp)
    ) {
        Image(
            painter = painterResource(
                id = button.lockIcon
            ),
            contentDescription = "Ring button content",
            modifier = Modifier.size(30.dp)
        )

    }
}

@Preview
@Composable
fun MyLongBlueButtonPreview() {
    LongButton(button = blueUIButton)
}

@Preview
@Composable
fun MyRingButtonPreview() {
    RingButton(button = UiButton.LockButton.OPENED)
}

val blueUIButton = UiButton.LongButton(
    action = {},
    backGroundColor = Color.Blue,
    textColor = Color.White,
    buttonText = "Активировать Ключ"
)

val greyUIButton = UiButton.LongButton(
    action = {},
    backGroundColor = Color.White,
    textColor = Color.Blue,
    buttonText = "Оплатить добавленные ключи"
)

val squaredButton = UiButton.SquaredButton(
    action = {},
    backGroundColor = Color.Gray,
    iconColor = Color.Blue,
    icon = R.drawable.ic_launcher_background
)
