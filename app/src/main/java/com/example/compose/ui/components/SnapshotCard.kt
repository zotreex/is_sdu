package com.example.compose.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.compose.R
import com.example.compose.ui.models.UiButton
import com.example.compose.ui.theme.ComposeTheme

@Composable
fun SnapshotCard(
    title: String = "The card view",
    subtitle: String = "Subtitle card view",
    targetImage: Painter = painterResource(id = R.drawable.ic_launcher_foreground),
    buttonImage: Painter = painterResource(id = R.drawable.ic_baseline_adb_24)
) {
    Card(
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp),
        backgroundColor = Color.LightGray,
        modifier = Modifier
            .padding(16.dp)
            .height(200.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .wrapContentHeight()
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Bottom,
        ) {

            Image(
                targetImage,
                contentDescription = "Icon for card view",
                Modifier
                    .weight(3.5F)
                    .height(280.dp)
                    .fillMaxWidth()
            )

            Row(
                Modifier
                    .weight(1F)
                    .padding(horizontal = 4.dp)
            ) {
                Column(Modifier.weight(2F)) {
                    Text(
                        textAlign = TextAlign.Start,
                        text = title,
                        color = Color.Black,
                        style = MaterialTheme.typography.subtitle1
                    )
                    Text(
                        text = subtitle,
                        color = Color.Gray,
                        textAlign = TextAlign.Start,
                        style = MaterialTheme.typography.subtitle1
                    )
                }

                RingButton(button = UiButton.LockButton.CLOSED)
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun CardPreview() {
    ComposeTheme {
        SnapshotCard()
    }
}
