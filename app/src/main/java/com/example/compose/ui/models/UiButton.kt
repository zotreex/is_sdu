package com.example.compose.ui.models

import androidx.compose.ui.graphics.Color
import com.example.compose.R

sealed class UiButton {
    class LongButton(
        val action: () -> Unit = {},
        val buttonText: String,
        val backGroundColor: Color = Color.Blue,
        val textColor: Color = Color.White
    )

    class SquaredButton(
        val action: () -> Unit,
        val icon: Int,
        val backGroundColor: Color,
        val iconColor: Color
    )

    enum class LockButton(val lockIcon: Int){
        OPENED(R.drawable.ic_baseline_lock_24),
        CLOSED(R.drawable.ic_baseline_lock_open_24)
    }
}
