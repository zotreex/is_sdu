package com.example.compose.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.compose.R
import com.example.compose.ui.theme.ComposeTheme

@Composable
fun SellerListItem(
    title: String = "The card view",
    targetImage: Painter = painterResource(id = R.drawable.ic_launcher_foreground),
) {
    Card(
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp),
        backgroundColor = Color.LightGray,
        modifier = Modifier
            .padding(16.dp)
            .height(200.dp)
            .width(160.dp)
    ) {
        Column(
            modifier = Modifier
                .wrapContentHeight()
                .fillMaxWidth()
                .padding(8.dp),
            verticalArrangement = Arrangement.Bottom,
        ) {

            Text(
                text = title,
                color = Color.Black,
                style = MaterialTheme.typography.subtitle2
            )

            Image(
                targetImage,
                contentDescription = "Icon for card view",
                Modifier
                    .padding(top = 8.dp)
                    .weight(3.5F)
                    .height(280.dp)
                    .fillMaxWidth()
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
fun SellerListPreview() {
    ComposeTheme {
        SellerListItem()
    }
}
