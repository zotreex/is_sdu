package com.example.compose.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.compose.R

@Composable
fun PriceListItem(isActive: Boolean, title: String, price: String, discount: String? = null) {
    Column(
        modifier = Modifier
            .clip(RoundedCornerShape(14.dp))
            .border(
                width = 2.dp,
                color = colorResource(id = R.color.basic_silver),
                shape = RoundedCornerShape(14.dp)
            )
            .background(colorResource(id = if (isActive) R.color.basic_silver else R.color.white))
            .padding(16.dp)
    ) {
        Text(
            text = title,
            maxLines = 2,
            modifier = Modifier
                .padding(bottom = 10.dp)
                .width(168.dp),
            color = colorResource(id = if (isActive) R.color.basic_content else R.color.basic_gray)
        )
        Row {
            Text(
                text = price,
                color = colorResource(id = if (isActive) R.color.brand else R.color.basic_gray),
                style = TextStyle(textDecoration = TextDecoration.LineThrough),
                modifier = Modifier.padding(end = 10.dp)
            )
            if (discount != null) {
                Text(
                    text = discount,
                    color = colorResource(id = R.color.black),
                )
            }
        }
    }
}

@Preview
@Composable
fun PreviewPriceListItem() {
    PriceListItem(false, title = "Месяц", price = "49 ₽", discount = "350 ₽")
}