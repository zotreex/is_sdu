package com.example.compose.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.compose.R

@Composable
fun ServiceListItem(iconPainter: Painter, title: String) {
    Column(
        modifier = Modifier
            .padding(24.dp)
            .background(colorResource(id = R.color.white)),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Surface(
            modifier = Modifier
                .clip(RoundedCornerShape(12))
                .background(colorResource(id = R.color.basic_silver))
                .padding(19.dp),
            color = colorResource(id = R.color.basic_silver)
        ) {
            Image(
                painter = iconPainter,
                contentDescription = title,
                modifier = Modifier.size(29.dp)
            )
        }
        Row(
            modifier = Modifier.padding(6.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(text = title)
        }
    }
}

@Preview
@Composable
fun Preview() {
    ServiceListItem(
        iconPainter = painterResource(id = R.drawable.ic_baseline_account_circle_24),
        title = "Архив"
    )
}

