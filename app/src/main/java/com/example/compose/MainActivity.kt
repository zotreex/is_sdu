package com.example.compose

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import coil.compose.rememberAsyncImagePainter
import com.example.compose.data.model.Data
import com.example.compose.data.model.Type
import com.example.compose.data.repository.ComponentRepository
import com.example.compose.data.utils.DataState
import com.example.compose.ui.components.LongButton
import com.example.compose.ui.components.SellerListItem
import com.example.compose.ui.components.SnapshotCard
import com.example.compose.ui.models.UiButton
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            lifecycleScope.launch {
                ComponentRepository()
                    .test()
                    .catch { setContent { ShowError() } }
                    .collect {
                        when (it) {
                            is DataState.Error -> setContent { ShowError() }
                            is DataState.Loading -> setContent { ShowLoading() }
                            is DataState.Success -> {
                                setContent {
                                    SetView(
                                        data = it.data.data,
                                    )
                                }
                            }
                        }
                    }
            }
        }
    }
}

@Composable
private fun ShowError() {
    Text(text = "There is a Problem")
}

@Composable
private fun ShowLoading() {
    CircularProgressIndicator()
}

@Composable
private fun SetView(
    data: List<Data>
) {
    LazyColumn() {
        item {
            data.forEach { value ->
                CheckUiType(value)
            }
        }
    }
}

@Composable
private fun CheckUiType(
    value: Data
) {
    when (value.type) {
        Type.BANNER -> Banner(value.title.orEmpty(), value.subtitle.orEmpty())
        Type.DOMOFON_CARD -> SnapshotCard(value.title.orEmpty(), value.subtitle.orEmpty())
        Type.BUTTON -> LongButton(
            UiButton.LongButton(
                buttonText = value.title.orEmpty(),
            )
        )
        Type.SELLING_BLOCK -> {
            LazyRow() {
                item {
                    value.items.forEach {
                        val painter = rememberAsyncImagePainter(it.image)
                        SellerListItem(it.title.orEmpty(), painter)
                    }
                }
            }
        }
        Type.SELLING_CARD -> {
            LazyRow() {
                item {
                    value.items.forEach {
                        val painter = rememberAsyncImagePainter(it.image)
                        SellerListItem(it.title.orEmpty(), painter)
                    }
                }
            }
        }
        Type.UNKNOWN -> Spacer(modifier = Modifier.requiredSize(0.dp))
        else -> Spacer(modifier = Modifier.requiredSize(0.dp))
    }
}
