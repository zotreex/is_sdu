package com.example.compose.data.repository

import com.example.compose.data.api.RetrofitBuilder
import com.example.compose.data.model.ScreenApiModel
import com.example.compose.data.utils.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*

class ComponentRepository {

    fun test(): Flow<DataState<ScreenApiModel>> = flow {
        try {
            val test = RetrofitBuilder.apiService.getScreen()
            emit(DataState.Success(test))
        } catch (
            exception: Exception
        ) {
            emit(DataState.Error(exception))
        }
    }.flowOn(Dispatchers.IO).onStart {
        emit(DataState.Loading)
    }
}
