package com.example.compose.data.model

import android.icu.text.CaseMap
import com.google.gson.annotations.SerializedName

class ScreenApiModel {
    @SerializedName("data")
    val data: List<Data> = emptyList()
}

data class Data(
    @SerializedName("items") val items: List<Data> = emptyList(),
    @SerializedName("type") var type: Type? = Type.UNKNOWN,
    @SerializedName("title") var title: String? = null,
    @SerializedName("subtitle") var subtitle: String? = null,
    @SerializedName("image") var image: String? = null,
)

enum class Type(val value:String) {
    @SerializedName("banner")
    BANNER("banner"),
    @SerializedName("domofon_card")
    DOMOFON_CARD("domofon_card"),
    @SerializedName("button")
    BUTTON("button"),
    @SerializedName("selling_block")
    SELLING_BLOCK("selling_block"),
    @SerializedName("selling_card")
    SELLING_CARD("selling_card"),
    @SerializedName("unknown")
    UNKNOWN("unknown")
}