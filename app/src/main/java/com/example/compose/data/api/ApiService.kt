package com.example.compose.data.api

import com.example.compose.data.model.ScreenApiModel
import retrofit2.http.GET

interface ApiService {

    @GET("ui.json")
    suspend fun getScreen(): ScreenApiModel
}